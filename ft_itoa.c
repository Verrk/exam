/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 13:31:43 by cpestour          #+#    #+#             */
/*   Updated: 2015/11/26 18:48:31 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int ft_abs(int nbr)
{
	if (nbr < 0)
		return (-nbr);
	return (nbr);
}

int ft_nbrlen(int nbr)
{
	int i;

	i = 1;
	if (nbr < 0)
		i++;
	while (nbr > 9 || nbr < -9)
	{
		nbr /= 10;
		i++;
	}
	return (i);
}

char *ft_itoa(int nbr)
{
	char *str;
	int i;

	i = ft_nbrlen(nbr);
	str = malloc(i + 1);
	str[i--] = '\0';
	if (nbr == 0)
		str[0] = '0';
	if (nbr < 0)
		str[0] = '-';
	while (nbr)
	{
		str[i--] = ft_abs(nbr % 10) + '0';
		nbr /= 10;
	}
	return (str);
}

char *ft_strrev(char *str)
{
	char tmp;
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[j])
		j++;
	j--;
	while (i < j)
	{
		tmp = str[i];
		str[i] = str[j];
		str[j] = tmp;
		i++;
		j--;
	}
	return (str);
}

char *ft_itoa_base(int nbr, int base)
{
	char *str;
	int i;
	int n;

	str = malloc(33);
	i = 0;
	while (i < 33)
		str[i++] = 0;
	i = 0;
	while (nbr)
	{
		n = nbr % base;
		if (n < 10)
			str[i] = n + '0';
		else
			str[i] = n - 10 + 'A';
		i++;
		nbr /= base;
	}
	return (ft_strrev(str));
}

int main()
{
	printf("%s\n", ft_itoa_base(214748347, 16));
	return (0);
}
