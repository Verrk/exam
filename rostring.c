/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 14:41:32 by cpestour          #+#    #+#             */
/*   Updated: 2015/11/24 15:22:01 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putstr(char *str)
{
	while (*str)
		write(1, str++, 1);
}

int ft_isspace(char c)
{
	if (c == ' ' || c == '\t')
		return (1);
	return (0);
}

void ft_skipspace(char **s)
{
	while (**s && ft_isspace(**s))
		(*s)++;
}

void rostring(char *str)
{
	char *s;

	ft_skipspace(&str);
	s = str;
	if (*s == '\0')
		return ;
	while (*str && !ft_isspace(*str))
		str++;
	if (ft_isspace(*str))
	{
		*str = '\0';
		str++;
	}
	ft_skipspace(&str);
	while (*str)
	{
		while (*str && !ft_isspace(*str))
		{
			write(1, str, 1);
			str++;
		}
		write(1, " ", 1);
		ft_skipspace(&str);
	}
	ft_putstr(s);
}

int main(int ac, char **av)
{
	if (ac == 2)
		rostring(av[1]);
	ft_putstr("\n");
	return (0);
}
