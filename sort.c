/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 18:50:20 by cpestour          #+#    #+#             */
/*   Updated: 2015/11/26 19:05:47 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

typedef struct s_list
{
	int data;
	struct s_list *next;
} t_list;

void swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

t_list *sort_list(t_list *lst, int (*cmp)(int, int))
{
	t_list *a;
	t_list *b;

	if (!lst)
		return (0);
	a = lst;
	while (a->next)
	{
		b = lst;
		while (b->next)
		{
			if (!cmp(b->data, b->next->data))
				swap(&(b->data), &(b->next->data));
			b = b->next;
		}
		a = a->next;
	}
	return (lst);
}

int	*sort_int_tab(int *tab, int size)
{
	int i;
	int j;

	i = 0;
	while (i < size - 1)
	{
		j = 0;
		while (j < size - 1)
		{
			if (tab[j] > tab[j + 1])
				swap(&tab[j], &tab[j + 1]);
			j++;
		}
		i++;
	}
	return (tab);
}

int croi(int a, int b)
{
	return (a <= b);
}

int main()
{
	t_list *a, *b, *c, *d, *e, *sort;
	a = malloc(sizeof(t_list));
	b = malloc(sizeof(t_list));
	c = malloc(sizeof(t_list));
	d = malloc(sizeof(t_list));
	e = malloc(sizeof(t_list));
	a->data = 50;
	a->next = b;
	b->data = 21;
	b->next = c;
	c->data = 9;
	c->next = d;
	d->data = 4;
	d->next = e;
	e->data = 1;
	e->next = NULL;
	sort = sort_list(a, croi);
	while (sort)
	{
		t_list *tmp;
		printf("%d\n", sort->data);
		tmp = sort;
		sort = sort->next;
		free(tmp);
	}
	int *tab, i;
	tab = malloc(5);
	tab[0] = 50;
	tab[1] = 90;
	tab[2] = 64;
	tab[3] = 400;
	tab[4] = 1;
	tab = sort_int_tab(tab, 5);
	printf("\n\n");
	for (i = 0; i < 5; i++)
		printf("%d\n", tab[i]);
	return (0);
}
