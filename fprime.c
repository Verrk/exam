/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 18:49:19 by cpestour          #+#    #+#             */
/*   Updated: 2016/01/05 07:26:27 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

void	ft_putnbr(int nb)
{
	char c;

	if (nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	else
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
}

int		ft_is_prime(int nb)
{
	int	i;

	if (nb == 2)
		return (1);
	if (nb <= 1 || nb % 2 == 0)
		return (0);
	i = 3;
	while (i * i <= nb)
	{
		if (nb % i == 0)
			return (0);
		i += 2;
	}
	return (1);
}

void fprime(int nb)
{
	int i;

	i = 2;
	if (nb == 0)
		return ;
	while (i <= nb / i)
	{
		if (nb % i == 0)
		{
			ft_putnbr(i);
			write(1, "*", 1);
			nb /= i;
		}
		else
		{
			i++;
			while (!ft_is_prime(i))
				i++;
		}
	}
	ft_putnbr(nb);
}

#include <stdio.h>
int main(int ac, char **av)
{
	int i;

	if (ac == 2)
	{
		i = atoi(av[1]);
		if (i >= 2)
			fprime(i);
	}
	write(1, "\n", 1);
	return (0);
}
