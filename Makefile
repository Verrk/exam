#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/26 18:43:31 by cpestour          #+#    #+#              #
#    Updated: 2015/11/26 18:49:03 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
CFLAGS=-Wall -Werror -Wextra

all:
	@echo "Command are:"
	@echo "\tmake itoa (ft_itoa + ft_itoa_base)"
	@echo "\tmake rostring"
	@echo "\tmake fprime"
	@echo "\tmake sort (sort_list + sort_int_tab)"

itoa: ft_itoa.c
	$(CC) -o $@ $^ $(CFLAGS)

rostring: rostring.c
	$(CC) -o $@ $^ $(CFLAGS)

fprime: fprime.c
	$(CC) -o $@ $^ $(CFLAGS)

sort: sort.c
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -f *~

fclean: clean
	rm -f a.out itoa rostring fprime sort
